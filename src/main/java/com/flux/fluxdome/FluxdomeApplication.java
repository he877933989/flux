package com.flux.fluxdome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.netty.tcp.TcpServerConfig;
import springfox.documentation.oas.annotations.EnableOpenApi;

import javax.annotation.PostConstruct;

@EnableOpenApi
@SpringBootApplication
public class FluxdomeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FluxdomeApplication.class, args);
	}

}
