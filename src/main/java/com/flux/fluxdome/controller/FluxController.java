package com.flux.fluxdome.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Api(tags = "初始化类")
@RestController
public class FluxController {

    @ApiOperation(value = "测试GETAPI", notes = "不作任何处理")
    @GetMapping("/index")
    public Mono<String> getStr() {
        return Mono.justOrEmpty("hello world");
    }

    @ApiOperation(value = "测试POSTAPI", notes = "传入什么参数就返回什么参数")
    @PostMapping("/inpost")
    public Mono<String> po(
            @RequestBody String param
    ) {
        return Mono.justOrEmpty(param);
    }

}
